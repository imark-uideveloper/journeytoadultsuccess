// Preloader 
jQuery(function (jQuery) {
    //Preloader
    var preloader = jQuery('.preloader');
    jQuery(window).load(function () {
        preloader.remove();
    });
});
// Wow 
  // grab the initial top offset of the navigation 
  var stickyNavTop = $('body').offset().top;
  // our function that decides weather the navigation bar should have "fixed" css position or not.
  var stickyNav = function () {
      var scrollTop = $(window).scrollTop(); // our current vertical position from the top

      // if we've scrolled more than the navigation, change its position to fixed to stick to top,
      // otherwise change it back to relative
      if (scrollTop > 300) {
          $('header1').addClass('sticky animated fadeInDown');
      } else {
          $('header1').removeClass('sticky fadeInDown');
      }

  };

  stickyNav();
  // and run it again every time you scroll
  $(window).scroll(function () {
      stickyNav();
  });




wow = new WOW({
    boxClass: 'wow', // default
    animateClass: 'animated', // default
    offset: 0, // default
    mobile: false, // default
    live: true // default
})
wow.init();
// Bootstrap Slider 
jQuery('.carousel').carousel({
    interval: 20000
});

/**** owl carousel ****/
jQuery(document).ready(function () {
    var owl = jQuery("#testimonial-demo");
    owl.owlCarousel({
        itemsCustom: [
                [767, 2]
                , [992, 4]
                , [1200, 4]
                , [1500, 4]]
        , navigation: true
        , pagination: true
        , slideSpeed: 1000
        , scrollPerPage: true
    });
    
      jQuery('.cstm_efct_line').append("<span class='lines top'></span><span class='lines right'></span><span class='lines bottom'></span><span class='lines left'></span>");
            jQuery('.cstm_efct_line').attr('cstmline', '');
});




    /**** Textarea First Letter Capital ****/
jQuery('textarea.form_control').on('keypress', function(event) {
  
var $this = jQuery(this),
thisVal = $this.val(),
FLC = thisVal.slice(0, 1).toUpperCase();
con = thisVal.slice(1, thisVal.length);
jQuery(this).val(FLC + con);
});

/**** Counter ****/

jQuery('.counter').each(function() {
  var jQuerythis = jQuery(this),
      countTo = jQuerythis.attr('data-count');
  
  jQuery({ countNum: jQuerythis.text()}).animate({
    countNum: countTo
  },

  {

    duration: 8000,
    easing:'linear',
    step: function() {
      jQuerythis.text(Math.floor(this.countNum));
    },
    complete: function() {
      jQuerythis.text(this.countNum);
      //alert('finished');
    }
  });

  });  
  
  